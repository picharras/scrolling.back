'use strict'
const Hapi = require('@hapi/hapi')
const routes = require('./routes')
const hapiAuthJWT = require('hapi-auth-jwt2')
const responseHandler = require('./libs/responseHandler')

const people = [{ id: '4321', email: 'hi@josuecamara.me' }]

const Server = function () {
  this.__server = Hapi.server({
    port: process.env.API_PORT,
    host: process.env.API_HOST,
    routes: {
      cors: {
        origin: ['*'],
        maxAge: 60,
        credentials: true
      }
    }
  })
}

Server.prototype.init = async function () {
  await this.__server.register(hapiAuthJWT)
  await this.__server.initialize()

  this.__server.auth.strategy('jwt', 'jwt', {
    key: process.env.API_JWT_SECRET_KEY,
    validate: async (decoded, request, h) => {
      const credentials = people.find(row => row.id === decoded.id)

      if (credentials) {
        return { isValid: true, credentials }
      }
      return { isValid: false }
    },
    verifyOptions: {
      issuer: 'josuecamara.me',
      algorithms: ['HS256']
    }
  })

  this.__server.route(routes)
  this.__server.ext('onPreResponse', responseHandler)
  return this.__server
}

Server.prototype.start = async function () {
  await this.__server.start()
  console.log('Server running on %s', this.__server.info.uri)
  return this.__server
}

process.on('unhandledRejection', (error) => {
  console.log(error)
  process.exit(1)
})

module.exports = Server
