module.exports = (request, h) => {
  const { response } = request
  if (!response.isBoom) return h.continue

  const data = response.data || {}
  const message = response.output.payload.message
  const kind = response.output.payload.error.replace(/\s/g, '')
  const errors = [{ message, kind, path: data.path, value: data.value }]

  return h.response({ errors }).code(response.output.statusCode)
}
