const path = require('path')
const Server = require('./server')

const env = process.env.NODE_ENV || 'development'

if (env !== 'production') {
  const envPath = path.resolve(process.cwd(), `.env.${env}`)
  require('dotenv').config({ path: envPath })
}

const server = new Server()

const startServer = async (server) => {
  await server.init()
  await server.start()
  return server
}

startServer(server)
