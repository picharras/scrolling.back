const Joi = require('joi')
const Boom = require('@hapi/boom')
const JWT = require('jsonwebtoken')
const textToImage = require('text-to-image')

const routes = [
  {
    method: 'GET',
    path: '/images',
    options: {
      auth: 'jwt',
      handler: async (request, h) => {
        const { query } = request

        const offset = (query.page - 1) * 10
        const result = { docs: [], total: 1000, page: query.page, pages: 100 }

        if (query.page > 100) return result

        for (let i = 1; i <= 10; i++) {
          const title = `Image ${offset + i}`
          const src = await textToImage.generate(title, {
            maxWidth: 512,
            customHeight: 400,
            fontSize: 90,
            fontFamily: 'Arial',
            textAlign: 'center',
            margin: 5,
            bgColor: '#007bff',
            textColor: '#f8f9fa'
          })

          result.docs.push({ src, title, id: offset + i })
        }
        return result
      },
      validate: {
        query: Joi.object({
          page: Joi.number().default(1)
        })
      }
    }
  }, {
    method: 'POST',
    path: '/signin',
    options: {
      handler: async (request, h) => {
        const { payload } = request
        const user = { id: '4321', email: 'hi@josuecamara.me', password: 'letsrock!' }

        const obj = Object.assign({}, user)
        delete obj.password

        if (payload.email === user.email && payload.password === user.password) {
          const token = JWT.sign(
            obj,
            process.env.API_JWT_SECRET_KEY,
            {
              expiresIn: '100d',
              issuer: 'josuecamara.me'
            })
          return { user, token }
        } else {
          throw Boom.unauthorized('invalid email or password')
        }
      },
      validate: {
        payload: Joi.object({
          email: Joi.string(),
          password: Joi.string()
        })
      }
    }
  }
]

module.exports = routes
