# scrolling.back
Scrolling.back is a tiny API made in HapiJS which serves images randomly generate

### Project setup
In order to run locally this project, you'll need to install the following software:

- Nodejs 14.15.x
- node module nodemon
- Git

Once you have installed all dependencies, clone this project:
```
git clone git@gitlab.com:picharras/scrolling.back.git
```

Then go to the project and install the dependencies:
```
npm install
```

### Setup for running in local
Before you can run the project it's necessary to set the environment variables, you can find them inside the file `env.example`. You only need to create a copy named `.env.development` and set other values if you desire.

Use this command to copy the file:
```
cp env.example .env.development
```

Then run the project
```
nodemon index.js
```

### Endpoints
There are two endpoints:
- `/sigin`. Allows you to create an token which is used in any call for asking images. Don't forget use it from your frontend project.
- `/images`. Returns randomly created images, this `endpoint` has a max pagination of 100 pages(1000 images)