process.env.NODE_ENV = 'test'

const path = require('path')
const chai = require('chai')
const { expect } = chai

const env = process.env.NODE_ENV

if (env !== 'production') {
  const envPath = path.resolve(process.cwd(), `.env.${env}`)
  require('dotenv').config({ path: envPath })
}

const Server = require('../server')

describe('Testing API', () => {
  let server = null
  before(async () => {
    const srv = new Server()
    server = await srv.init()
  })

  describe('Testing signin', () => {
    it('should fail trying generate an token using invalid credentials', async () => {
      const response = await server.inject({
        method: 'post',
        url: '/signin',
        payload: { email: 'hi@jos.me', password: '1234' }
      })

      const body = JSON.parse(response.payload)
      expect(body.errors).eql([{ message: 'invalid email or password', kind: 'Unauthorized' }])
      expect(response.statusCode).to.eql(401)
    })

    it('should generate a token', async () => {
      const response = await server.inject({
        method: 'post',
        url: '/signin',
        payload: { email: 'hi@josuecamara.me', password: 'letsrock!' }
      })

      const body = JSON.parse(response.payload)
      expect(body.user.id).to.eql('4321')
      expect(body.user.email).to.eql('hi@josuecamara.me')
      expect(response.statusCode).to.eql(200)
    })
  })

  describe('Testing images endpoint', () => {
    let token = null
    before(async () => {
      const response = await server.inject({
        method: 'post',
        url: '/signin',
        payload: { email: 'hi@josuecamara.me', password: 'letsrock!' }
      })
      const body = JSON.parse(response.payload)
      token = body.token
    })

    it('should fail asking a page without token', async () => {
      const response = await server.inject({
        method: 'get',
        url: '/images',
        payload: { email: 'hi@jos.me', password: '1234' }
      })

      const body = JSON.parse(response.payload)
      expect(body.errors).to.eql([{ message: 'Missing authentication', kind: 'Unauthorized' }])
      expect(response.statusCode).to.eql(401)
    })

    it('should fetch page 10', async () => {
      const response = await server.inject({
        method: 'get',
        url: '/images?page=10',
        headers: { Authorization: token }
      })

      const body = JSON.parse(response.payload)
      expect(body.docs).to.be.an('Array').lengthOf(10)
      expect(body.page).to.eql(10)
      expect(body.pages).to.eql(100)
      expect(body.total).to.eql(1000)
      expect(response.statusCode).to.eql(200)
    })
  })
})
